from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.views import generic
from .forms import LoginForm

class IndexView(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {"name": "Ray Parra"}
        return render(request, self.template_name, self.context)

class LoginView(generic.View):
    template_name = 'home/login.html'

    def get(self, request):
        form = LoginForm()
        context = {'form': form}
        return render(request, self.template_name, context)

    def post(self, request):
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None and user.is_superuser:
                login(request, user)
                return redirect('home:index')

        context = {'form': form}
        return render(request, self.template_name, context)

def LogoutView(request):
    logout(request)
    return redirect('home:index')

    