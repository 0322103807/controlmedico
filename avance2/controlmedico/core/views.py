from django.shortcuts import render
from django.db.models import Q
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from .forms import PuestoForm, EmpleadoForm, AlergiaForm, DepartamentoForm, MedicamentoForm, SuministroForm, MedicamentoConsultaForm
from .forms import AlergiaEmpleadoForm, MediEfecSecuForm, MediContraForm, MediFormaAdmiForm, SuministroConsultaForm
from .forms import MedicoForm, ConsultaForm, EfectosSecundariosForm, ContraindicacionForm, FormaAdministracionForm, MedicamentoConsulta
from .models import Puesto, Empleado, Alergia, Departamento, Medicamento, Suministro, Medico, Consulta, MediEfecSecu
from .models import EfectosSecundarios, Contraindicacion, FormaAdministracion, AlergiaEmpleado, MediContra, MediFormaAdmi
from .models import SuministroConsulta
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User

# Alergia
class CreateAlergia(LoginRequiredMixin, CreateView):
    template_name = "core/create_alergia.html"
    model = Alergia
    form_class = AlergiaForm
    success_url = reverse_lazy('core:list_alergia')

class ListAlergia(LoginRequiredMixin, ListView):
    template_name = "core/list_alergia.html"  
    model = Alergia
    context_object_name = 'Alergias'
    login_url = reverse_lazy("home:login")

class DetailAlergia(LoginRequiredMixin, DetailView):
    template_name = "core/detail_alergia.html"
    model = Alergia
    context_object_name = 'alergia'

class UpdateAlergia(LoginRequiredMixin, UpdateView):
    template_name = "core/update_alergia.html"
    model = Alergia
    form_class = AlergiaForm
    success_url = reverse_lazy('core:list_alergia')

class DeleteAlergia(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_alergia.html"
    model = Alergia
    success_url = reverse_lazy('core:list_alergia')
    
# Puesto
class CreatePuesto(LoginRequiredMixin, CreateView):
    template_name = "core/create_puesto.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy('core:list_puesto')

class ListPuesto(LoginRequiredMixin, ListView):
    template_name = "core/list_puesto.html"  
    model = Puesto
    context_object_name = 'Puestos'
    login_url = reverse_lazy("home:login")

class DetailPuesto(LoginRequiredMixin, DetailView):
    template_name = "core/detail_puesto.html"
    model = Puesto
    context_object_name = 'puesto'

class UpdatePuesto(LoginRequiredMixin, UpdateView):
    template_name = "core/update_puesto.html"
    model = Puesto
    form_class = PuestoForm
    success_url = reverse_lazy('core:list_puesto')

class DeletePuesto(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_puesto.html"
    model = Puesto
    success_url = reverse_lazy('core:list_puesto')

# Departamento    
class CreateDepartamento(LoginRequiredMixin, CreateView):
    template_name = "core/create_departamento.html"
    model = Departamento
    form_class = DepartamentoForm
    success_url = reverse_lazy('core:list_departamento')

class ListDepartamento(LoginRequiredMixin, ListView):
    template_name = "core/list_departamento.html"  
    model = Departamento
    context_object_name = 'Departamentos'
    login_url = reverse_lazy("home:login")

class DetailDepartamento(LoginRequiredMixin, DetailView):
    template_name = "core/detail_departamento.html"
    model = Departamento
    context_object_name = 'departamento'

class UpdateDepartamento(LoginRequiredMixin, UpdateView):
    template_name = "core/update_departamento.html"
    model = Departamento
    form_class = DepartamentoForm
    success_url = reverse_lazy('core:list_departamento')

class DeleteDepartamento(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_departamento.html"
    model = Departamento
    success_url = reverse_lazy('core:list_departamento')

# Empleado
class CreateEmpleado(LoginRequiredMixin, CreateView):
    template_name = "core/create_empleado.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy('core:list_empleado')

class ListEmpleado(LoginRequiredMixin, ListView):
    template_name = "core/list_empleado.html"  
    model = Empleado
    context_object_name = 'Empleados'
    login_url = reverse_lazy("home:login")

class DetailEmpleado(LoginRequiredMixin, DetailView):
    template_name = "core/detail_empleado.html"
    model = Empleado
    context_object_name = 'empleado'

class UpdateEmpleado(LoginRequiredMixin, UpdateView):
    template_name = "core/update_empleado.html"
    model = Empleado
    form_class = EmpleadoForm
    success_url = reverse_lazy('core:list_empleado')

class DeleteEmpleado(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_empleado.html"
    model = Empleado
    success_url = reverse_lazy('core:list_empleado')

# Medicamento
class CreateMedicamento(LoginRequiredMixin, CreateView):
    template_name = "core/create_medicamento.html"
    model = Medicamento
    form_class = MedicamentoForm
    success_url = reverse_lazy('core:list_medicamento')

class ListMedicamento(LoginRequiredMixin, ListView):
    template_name = "core/list_medicamento.html"  
    model = Medicamento
    context_object_name = 'Medicamentos'
    login_url = reverse_lazy("home:login")

class DetailMedicamento(LoginRequiredMixin, DetailView):
    template_name = "core/detail_medicamento.html"
    model = Medicamento
    context_object_name = 'medicamento'

class UpdateMedicamento(LoginRequiredMixin, UpdateView):
    template_name = "core/update_medicamento.html"
    model = Medicamento
    form_class = MedicamentoForm
    success_url = reverse_lazy('core:list_medicamento')

class DeleteMedicamento(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_medicamento.html"
    model = Medicamento
    success_url = reverse_lazy('core:list_medicamento')
    
# Suministro
class CreateSuministro(LoginRequiredMixin, CreateView):
    template_name = "core/create_suministro.html"
    model = Suministro
    form_class = SuministroForm
    success_url = reverse_lazy('core:list_suministro')

class ListSuministro(LoginRequiredMixin, ListView):
    template_name = "core/list_suministro.html"  
    model = Suministro
    context_object_name = 'Suministros'
    login_url = reverse_lazy("home:login")

class DetailSuministro(LoginRequiredMixin, DetailView):
    template_name = "core/detail_suministro.html"
    model = Suministro
    context_object_name = 'suministro'

class UpdateSuministro(LoginRequiredMixin, UpdateView):
    template_name = "core/update_suministro.html"
    model = Suministro
    form_class = SuministroForm
    success_url = reverse_lazy('core:list_suministro')

class DeleteSuministro(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_suministro.html"
    model = Suministro
    success_url = reverse_lazy('core:list_suministro')
    
# Medico
class CreateMedico(LoginRequiredMixin, CreateView):
    template_name = "core/create_medico.html"
    model = Medico
    form_class = MedicoForm
    success_url = reverse_lazy('core:list_medico')

    def form_valid(self, form):
        response = super().form_valid(form)
        username = form.cleaned_data['cedula']
        password = form.cleaned_data['cedula']
        user = User.objects.create_user(username=username, password=password)
        user.is_superuser = True
        user.save()

        return response

class ListMedico(LoginRequiredMixin, ListView):
    template_name = "core/list_medico.html"  
    model = Medico
    context_object_name = 'Medicos'
    login_url = reverse_lazy("home:login")

class DetailMedico(LoginRequiredMixin, DetailView):
    template_name = "core/detail_medico.html"
    model = Medico
    context_object_name = 'medico'

class UpdateMedico(LoginRequiredMixin, UpdateView):
    template_name = "core/update_medico.html"
    model = Medico
    form_class = MedicoForm
    success_url = reverse_lazy('core:list_medico')

class DeleteMedico(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_medico.html"
    model = Medico
    success_url = reverse_lazy('core:list_medico')
    
#Consulta
class CreateConsulta(LoginRequiredMixin, CreateView):
    template_name = "core/create_consulta.html"
    model = Consulta
    form_class = ConsultaForm
    success_url = reverse_lazy('core:list_consulta')

    def form_valid(self, form):
        medico_instance, created = Medico.objects.get_or_create(cedula=self.request.user)

        form.instance.medico = medico_instance
        return super().form_valid(form)
    

class ListConsulta(LoginRequiredMixin, ListView):
    template_name = "core/list_consulta.html"
    model = Consulta
    context_object_name = 'Consultas'
    login_url = reverse_lazy("home:login")

class DetailConsulta(LoginRequiredMixin, DetailView):
    template_name = "core/detail_consulta.html"
    model = Consulta
    context_object_name = 'consulta'

class UpdateConsulta(LoginRequiredMixin, UpdateView):
    template_name = "core/update_consulta.html"
    model = Consulta
    form_class = ConsultaForm
    success_url = reverse_lazy('core:list_consulta')

class DeleteConsulta(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_consulta.html"
    model = Consulta
    success_url = reverse_lazy('core:list_consulta')

# EfectosSecundarios
class CreateEfectosSecundarios(LoginRequiredMixin, CreateView):
    template_name = "core/create_efectos_secundarios.html"
    model = EfectosSecundarios
    form_class = EfectosSecundariosForm
    success_url = reverse_lazy('core:list_efectos_secundarios')

class ListEfectosSecundarios(LoginRequiredMixin, ListView):
    template_name = "core/list_efectos_secundarios.html"
    model = EfectosSecundarios
    context_object_name = 'EfectosSecundarios'
    login_url = reverse_lazy("home:login")

class DetailEfectosSecundarios(LoginRequiredMixin, DetailView):
    template_name = "core/detail_efectos_secundarios.html"
    model = EfectosSecundarios
    context_object_name = 'efectos_secundarios'

class UpdateEfectosSecundarios(LoginRequiredMixin, UpdateView):
    template_name = "core/update_efectos_secundarios.html"
    model = EfectosSecundarios
    form_class = EfectosSecundariosForm
    success_url = reverse_lazy('core:list_efectos_secundarios')

class DeleteEfectosSecundarios(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_efectos_secundarios.html"
    model = EfectosSecundarios
    success_url = reverse_lazy('core:list_efectos_secundarios')

#Contraindicacion
class CreateContraindicacion(LoginRequiredMixin, CreateView):
    template_name = "core/create_contraindicacion.html"
    model = Contraindicacion
    form_class = ContraindicacionForm
    success_url = reverse_lazy('core:list_contraindicacion')

class ListContraindicacion(LoginRequiredMixin, ListView):
    template_name = "core/list_contraindicacion.html"
    model = Contraindicacion
    context_object_name = 'Contraindicaciones'
    login_url = reverse_lazy("home:login")

class DetailContraindicacion(LoginRequiredMixin, DetailView):
    template_name = "core/detail_contraindicacion.html"
    model = Contraindicacion
    context_object_name = 'contraindicacion'

class UpdateContraindicacion(LoginRequiredMixin, UpdateView):
    template_name = "core/update_contraindicacion.html"
    model = Contraindicacion
    form_class = ContraindicacionForm
    success_url = reverse_lazy('core:list_contraindicacion')

class DeleteContraindicacion(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_contraindicacion.html"
    model = Contraindicacion
    success_url = reverse_lazy('core:list_contraindicacion')

#FormaAdministracion
class CreateFormaAdministracion(LoginRequiredMixin, CreateView):
    template_name = "core/create_forma_administracion.html"
    model = FormaAdministracion
    form_class = FormaAdministracionForm
    success_url = reverse_lazy('core:list_forma_administracion')

class ListFormaAdministracion(LoginRequiredMixin, ListView):
    template_name = "core/list_forma_administracion.html"
    model = FormaAdministracion
    context_object_name = 'FormasAdministracion'
    login_url = reverse_lazy("home:login")

class DetailFormaAdministracion(LoginRequiredMixin, DetailView):
    template_name = "core/detail_forma_administracion.html"
    model = FormaAdministracion
    context_object_name = 'forma_administracion'

class UpdateFormaAdministracion(LoginRequiredMixin, UpdateView):
    template_name = "core/update_forma_administracion.html"
    model = FormaAdministracion
    form_class = FormaAdministracionForm
    success_url = reverse_lazy('core:list_forma_administracion')

class DeleteFormaAdministracion(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_forma_administracion.html"
    model = FormaAdministracion
    success_url = reverse_lazy('core:list_forma_administracion')
    
#AlergiaEmpleado

class CreateAlergiaEmpleado(LoginRequiredMixin, CreateView):
    template_name = "core/create_alergiaempleado.html"
    model = AlergiaEmpleado
    form_class = AlergiaEmpleadoForm
    success_url = reverse_lazy('core:list_alergiaempleado')

class ListAlergiaEmpleado(LoginRequiredMixin, ListView):
    template_name = "core/list_alergiaempleado.html"  
    model = AlergiaEmpleado
    context_object_name = 'AlergiaEmpleados'
    login_url = reverse_lazy("home:login")

    def get_queryset(self):
        query = self.request.GET.get('q')
        if query:
            return AlergiaEmpleado.objects.filter(
                Q(empleado__noempleado__icontains=query)
            )
        return AlergiaEmpleado.objects.all()

class DetailAlergiaEmpleado(LoginRequiredMixin, DetailView):
    template_name = "core/detail_alergiaempleado.html"
    model = AlergiaEmpleado
    context_object_name = 'alergiaempleado'

class UpdateAlergiaEmpleado(LoginRequiredMixin, UpdateView):
    template_name = "core/update_alergiaempleado.html"
    model = AlergiaEmpleado
    form_class = AlergiaEmpleadoForm
    success_url = reverse_lazy('core:list_alergiaempleado')

class DeleteAlergiaEmpleado(LoginRequiredMixin, DeleteView):
    template_name = "core/delete_alergiaempleado.html"
    model = AlergiaEmpleado
    success_url = reverse_lazy('core:list_alergiaempleado')
    
# MediEfecSecu
class CreateMediEfecSecu(CreateView):
    template_name = "core/create_medi_efec_secu.html"
    model = MediEfecSecu
    form_class = MediEfecSecuForm
    success_url = reverse_lazy('core:list_medi_efec_secu')

class ListMediEfecSecu(ListView):
    template_name = "core/list_medi_efec_secu.html"
    model = MediEfecSecu
    context_object_name = 'MediEfecSecus'

class DetailMediEfecSecu(DetailView):
    template_name = "core/detail_medi_efec_secu.html"
    model = MediEfecSecu
    context_object_name = 'medi_efec_secu'

class UpdateMediEfecSecu(UpdateView):
    template_name = "core/update_medi_efec_secu.html"
    model = MediEfecSecu
    form_class = MediEfecSecuForm
    success_url = reverse_lazy('core:list_medi_efec_secu')

class DeleteMediEfecSecu(DeleteView):
    template_name = "core/delete_medi_efec_secu.html"
    model = MediEfecSecu
    success_url = reverse_lazy('core:list_medi_efec_secu')
    
# MediContra
class CreateMediContra(CreateView):
    template_name = "core/create_medi_contra.html"
    model = MediContra
    form_class = MediContraForm
    success_url = reverse_lazy('core:list_medi_contra')

class ListMediContra(ListView):
    template_name = "core/list_medi_contra.html"
    model = MediContra
    context_object_name = 'MediContras'

class DetailMediContra(DetailView):
    template_name = "core/detail_medi_contra.html"
    model = MediContra
    context_object_name = 'medi_contra'

class UpdateMediContra(UpdateView):
    template_name = "core/update_medi_contra.html"
    model = MediContra
    form_class = MediContraForm
    success_url = reverse_lazy('core:list_medi_contra')

class DeleteMediContra(DeleteView):
    template_name = "core/delete_medi_contra.html"
    model = MediContra
    success_url = reverse_lazy('core:list_medi_contra')
    
# MediFormaAdmi
class CreateMediFormaAdmi(CreateView):
    template_name = "core/create_medi_forma_admi.html"
    model = MediFormaAdmi
    form_class = MediFormaAdmiForm
    success_url = reverse_lazy('core:list_medi_forma_admi')

class ListMediFormaAdmi(ListView):
    template_name = "core/list_medi_forma_admi.html"
    model = MediFormaAdmi
    context_object_name = 'MediFormaAdmis'

class DetailMediFormaAdmi(DetailView):
    template_name = "core/detail_medi_forma_admi.html"
    model = MediFormaAdmi
    context_object_name = 'medi_forma_admi'

class UpdateMediFormaAdmi(UpdateView):
    template_name = "core/update_medi_forma_admi.html"
    model = MediFormaAdmi
    form_class = MediFormaAdmiForm
    success_url = reverse_lazy('core:list_medi_forma_admi')

class DeleteMediFormaAdmi(DeleteView):
    template_name = "core/delete_medi_forma_admi.html"
    model = MediFormaAdmi
    success_url = reverse_lazy('core:list_medi_forma_admi')

# SuministroConsulta
class CreateSuministroConsulta(CreateView):
    template_name = "core/create_suministro_consulta.html"
    model = SuministroConsulta
    form_class = SuministroConsultaForm
    success_url = reverse_lazy('core:list_suministro_consulta')

class ListSuministroConsulta(ListView):
    template_name = "core/list_suministro_consulta.html"
    model = SuministroConsulta
    context_object_name = 'SuministroConsultas'

class DetailSuministroConsulta(DetailView):
    template_name = "core/detail_suministro_consulta.html"
    model = SuministroConsulta
    context_object_name = 'suministro_consulta'

class UpdateSuministroConsulta(UpdateView):
    template_name = "core/update_suministro_consulta.html"
    model = SuministroConsulta
    form_class = SuministroConsultaForm
    success_url = reverse_lazy('core:list_suministro_consulta')

class DeleteSuministroConsulta(DeleteView):
    template_name = "core/delete_suministro_consulta.html"
    model = SuministroConsulta
    success_url = reverse_lazy('core:list_suministro_consulta')
 
 #MedicamentoConsulta   
class CreateMedicamentoConsulta(CreateView):
    template_name = "core/create_medicamento_consulta.html"
    model = MedicamentoConsulta
    form_class = MedicamentoConsultaForm
    success_url = reverse_lazy('core:list_medicamento_consulta')

class ListMedicamentoConsulta(ListView):
    template_name = "core/list_medicamento_consulta.html"
    model = MedicamentoConsulta
    context_object_name = 'MedicamentoConsultas'

class DetailMedicamentoConsulta(DetailView):
    template_name = "core/detail_medicamento_consulta.html"
    model = MedicamentoConsulta
    context_object_name = 'medicamento_consulta'

class UpdateMedicamentoConsulta(UpdateView):
    template_name = "core/update_medicamento_consulta.html"
    model = MedicamentoConsulta
    form_class = MedicamentoConsultaForm
    success_url = reverse_lazy('core:list_medicamento_consulta')

class DeleteMedicamentoConsulta(DeleteView):
    template_name = "core/delete_medicamento_consulta.html"
    model = MedicamentoConsulta
    success_url = reverse_lazy('core:list_medicamento_consulta')