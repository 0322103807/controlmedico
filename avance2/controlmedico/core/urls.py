from django.urls import path
from . import views
from .views import CreateMedico, ListMedico


app_name = 'core' 

urlpatterns = [
    # URLs para Puesto
    path('puesto/create/', views.CreatePuesto.as_view(), name='create_puesto'),
    path('puesto/list/', views.ListPuesto.as_view(), name='list_puesto'),
    path('puesto/<int:pk>/', views.DetailPuesto.as_view(), name='detail_puesto'),
    path('puesto/update/<int:pk>/', views.UpdatePuesto.as_view(), name='update_puesto'),
    path('puesto/<int:pk>/delete/', views.DeletePuesto.as_view(), name='delete_puesto'),

    # URLs para Empleado
    path('empleado/create/', views.CreateEmpleado.as_view(), name='create_empleado'),
    path('empleado/list/', views.ListEmpleado.as_view(), name='list_empleado'),
    path('empleado/<int:pk>/', views.DetailEmpleado.as_view(), name='detail_empleado'),
    path('empleado/<int:pk>/update/', views.UpdateEmpleado.as_view(), name='update_empleado'),
    path('empleado/<int:pk>/delete/', views.DeleteEmpleado.as_view(), name='delete_empleado'),

    # URLs para Alergia
    path('alergia/create/', views.CreateAlergia.as_view(), name='create_alergia'),
    path('alergia/list/', views.ListAlergia.as_view(), name='list_alergia'),
    path('alergia/<int:pk>/', views.DetailAlergia.as_view(), name='detail_alergia'),
    path('alergia/<int:pk>/update/', views.UpdateAlergia.as_view(), name='update_alergia'),
    path('alergia/<int:pk>/delete/', views.DeleteAlergia.as_view(), name='delete_alergia'),

    # URLs para Departamento
    path('departamento/create/', views.CreateDepartamento.as_view(), name='create_departamento'),
    path('departamento/list/', views.ListDepartamento.as_view(), name='list_departamento'),
    path('departamento/<int:pk>/', views.DetailDepartamento.as_view(), name='detail_departamento'),
    path('departamento/<int:pk>/update/', views.UpdateDepartamento.as_view(), name='update_departamento'),
    path('departamento/<int:pk>/delete/', views.DeleteDepartamento.as_view(), name='delete_departamento'),

    # URLs para Medicamento
    path('medicamento/create/', views.CreateMedicamento.as_view(), name='create_medicamento'),
    path('medicamento/list/', views.ListMedicamento.as_view(), name='list_medicamento'),
    path('medicamento/<int:pk>/', views.DetailMedicamento.as_view(), name='detail_medicamento'),
    path('medicamento/<int:pk>/update/', views.UpdateMedicamento.as_view(), name='update_medicamento'),
    path('medicamento/<int:pk>/delete/', views.DeleteMedicamento.as_view(), name='delete_medicamento'),
    
    # URLs para Suministro
    path('suministro/create/', views.CreateSuministro.as_view(), name='create_suministro'),
    path('suministro/list/', views.ListSuministro.as_view(), name='list_suministro'),
    path('suministro/<int:pk>/', views.DetailSuministro.as_view(), name='detail_suministro'),
    path('suministro/<int:pk>/update/', views.UpdateSuministro.as_view(), name='update_suministro'),
    path('suministro/<int:pk>/delete/', views.DeleteSuministro.as_view(), name='delete_suministro'),

    # URLs para Medico
    path('medico/create/', views.CreateMedico.as_view(), name='create_medico'),
    path('medico/list/', views.ListMedico.as_view(), name='list_medico'),
    path('medico/<int:pk>/', views.DetailMedico.as_view(), name='detail_medico'),
    path('medico/<int:pk>/update/', views.UpdateMedico.as_view(), name='update_medico'),
    path('medico/<int:pk>/delete/', views.DeleteMedico.as_view(), name='delete_medico'),

    # URLs para Consulta
    path('consulta/create/', views.CreateConsulta.as_view(), name='create_consulta'),
    path('consulta/list/', views.ListConsulta.as_view(), name='list_consulta'),
    path('consulta/<int:pk>/', views.DetailConsulta.as_view(), name='detail_consulta'),
    path('consulta/<int:pk>/update/', views.UpdateConsulta.as_view(), name='update_consulta'),
    path('consulta/<int:pk>/delete/', views.DeleteConsulta.as_view(), name='delete_consulta'),
    
    # URLs para EfectosSecundarios
    path('efectos_secundarios/create/', views.CreateEfectosSecundarios.as_view(), name='create_efectos_secundarios'),
    path('efectos_secundarios/list/', views.ListEfectosSecundarios.as_view(), name='list_efectos_secundarios'),
    path('efectos_secundarios/<int:pk>/', views.DetailEfectosSecundarios.as_view(), name='detail_efectos_secundarios'),
    path('efectos_secundarios/<int:pk>/update/', views.UpdateEfectosSecundarios.as_view(), name='update_efectos_secundarios'),
    path('efectos_secundarios/<int:pk>/delete/', views.DeleteEfectosSecundarios.as_view(), name='delete_efectos_secundarios'),

    # URLs para Contraindicacion
    path('contraindicacion/create/', views.CreateContraindicacion.as_view(), name='create_contraindicacion'),
    path('contraindicacion/list/', views.ListContraindicacion.as_view(), name='list_contraindicacion'),
    path('contraindicacion/<int:pk>/', views.DetailContraindicacion.as_view(), name='detail_contraindicacion'),
    path('contraindicacion/<int:pk>/update/', views.UpdateContraindicacion.as_view(), name='update_contraindicacion'),
    path('contraindicacion/<int:pk>/delete/', views.DeleteContraindicacion.as_view(), name='delete_contraindicacion'),
    # URLs para FormaAdministracion
    path('forma_administracion/create/', views.CreateFormaAdministracion.as_view(), name='create_forma_administracion'),
    path('forma_administracion/list/', views.ListFormaAdministracion.as_view(), name='list_forma_administracion'),
    path('forma_administracion/<int:pk>/', views.DetailFormaAdministracion.as_view(), name='detail_forma_administracion'),
    path('forma_administracion/<int:pk>/update/', views.UpdateFormaAdministracion.as_view(), name='update_forma_administracion'),
    path('forma_administracion/<int:pk>/delete/', views.DeleteFormaAdministracion.as_view(), name='delete_forma_administracion'),

    # URLs para AlergiaEmpleado
    path('alergiaempleado/create/', views.CreateAlergiaEmpleado.as_view(), name='create_alergiaempleado'),
    path('alergiaempleado/list/', views.ListAlergiaEmpleado.as_view(), name='list_alergiaempleado'),
    path('alergiaempleado/<int:pk>/', views.DetailAlergiaEmpleado.as_view(), name='detail_alergiaempleado'),
    path('alergiaempleado/<int:pk>/update/', views.UpdateAlergiaEmpleado.as_view(), name='update_alergiaempleado'),
    path('alergiaempleado/<int:pk>/delete/', views.DeleteAlergiaEmpleado.as_view(), name='delete_alergiaempleado'),

    # URLs para MediEfecSecu
    path('medi_efec_secu/create/', views.CreateMediEfecSecu.as_view(), name='create_medi_efec_secu'),
    path('medi_efec_secu/list/', views.ListMediEfecSecu.as_view(), name='list_medi_efec_secu'),
    path('medi_efec_secu/<int:pk>/', views.DetailMediEfecSecu.as_view(), name='detail_medi_efec_secu'),
    path('medi_efec_secu/<int:pk>/update/', views.UpdateMediEfecSecu.as_view(), name='update_medi_efec_secu'),
    path('medi_efec_secu/<int:pk>/delete/', views.DeleteMediEfecSecu.as_view(), name='delete_medi_efec_secu'),

    # URLs para MediContra
    path('medi_contra/create/', views.CreateMediContra.as_view(), name='create_medi_contra'),
    path('medi_contra/list/', views.ListMediContra.as_view(), name='list_medi_contra'),
    path('medi_contra/<int:pk>/', views.DetailMediContra.as_view(), name='detail_medi_contra'),
    path('medi_contra/<int:pk>/update/', views.UpdateMediContra.as_view(), name='update_medi_contra'),
    path('medi_contra/<int:pk>/delete/', views.DeleteMediContra.as_view(), name='delete_medi_contra'),

    # URLs para MediFormaAdmi
    path('medi_forma_admi/create/', views.CreateMediFormaAdmi.as_view(), name='create_medi_forma_admi'),
    path('medi_forma_admi/list/', views.ListMediFormaAdmi.as_view(), name='list_medi_forma_admi'),
    path('medi_forma_admi/<int:pk>/', views.DetailMediFormaAdmi.as_view(), name='detail_medi_forma_admi'),
    path('medi_forma_admi/<int:pk>/update/', views.UpdateMediFormaAdmi.as_view(), name='update_medi_forma_admi'),
    path('medi_forma_admi/<int:pk>/delete/', views.DeleteMediFormaAdmi.as_view(), name='delete_medi_forma_admi'),

    # URLs para SuministroConsulta
    path('suministro_consulta/create/', views.CreateSuministroConsulta.as_view(), name='create_suministro_consulta'),
    path('suministro_consulta/list/', views.ListSuministroConsulta.as_view(), name='list_suministro_consulta'),
    path('suministro_consulta/<int:pk>/', views.DetailSuministroConsulta.as_view(), name='detail_suministro_consulta'),
    path('suministro_consulta/<int:pk>/update/', views.UpdateSuministroConsulta.as_view(), name='update_suministro_consulta'),
    path('suministro_consulta/<int:pk>/delete/', views.DeleteSuministroConsulta.as_view(), name='delete_suministro_consulta'),

    # URLs para MedicamentoConsulta
    path('medicamento_consulta/create/', views.CreateMedicamentoConsulta.as_view(), name='create_medicamento_consulta'),
    path('medicamento_consulta/list/', views.ListMedicamentoConsulta.as_view(), name='list_medicamento_consulta'),
    path('medicamento_consulta/<int:pk>/', views.DetailMedicamentoConsulta.as_view(), name='detail_medicamento_consulta'),
    path('medicamento_consulta/<int:pk>/update/', views.UpdateMedicamentoConsulta.as_view(), name='update_medicamento_consulta'),
    path('medicamento_consulta/<int:pk>/delete/', views.DeleteMedicamentoConsulta.as_view(), name='delete_medicamento_consulta'),
]
 