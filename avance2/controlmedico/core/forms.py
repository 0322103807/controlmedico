from django.utils import timezone
from django import forms
from .models import Puesto, Empleado, Alergia, Departamento, Medicamento, AlergiaEmpleado
from .models import Suministro, Medico, Consulta, EfectosSecundarios, Contraindicacion, FormaAdministracion, MediEfecSecu
from .models import MediContra, MediFormaAdmi, SuministroConsulta, MedicamentoConsulta
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm

class PuestoForm(forms.ModelForm):
    class Meta:
        model = Puesto
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del puesto'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del puesto'}),
        }

class EmpleadoForm(forms.ModelForm):
    class Meta:
        model = Empleado
        fields = ['noempleado', 'nombrePila', 'apPat', 'apMat', 'fnacimiento', 'puesto', 'departamento']
        widgets = {
            'noempleado': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de empleado'}),
            'nombrePila': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del empleado'}),
            'apPat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido paterno'}),
            'apMat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido materno'}),
            'fnacimiento': forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'puesto': forms.Select(attrs={'class': 'form-control'}),
            'departamento': forms.Select(attrs={'class': 'form-control'}),
        }

class AlergiaForm(forms.ModelForm):
    class Meta:
        model = Alergia
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la alergia'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción de la alergia'}),
        }

class DepartamentoForm(forms.ModelForm):
    class Meta:
        model = Departamento
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del Departamento'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del Departamento'}),
        }
        
class MedicamentoForm(forms.ModelForm):
    class Meta:
        model = Medicamento
        fields = ['nombre', 'descripcion', 'dosis', 'stock']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del medicamento'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del medicamento'}),
            'dosis': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Dosis del medicamento'}),
            'stock': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Stock del medicamento'}),
        }
        
class SuministroForm(forms.ModelForm):
    class Meta:
        model = Suministro
        fields = ['nombre', 'descripcion', 'stock']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del suministro'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del suministro'}),
            'stock': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Stock del suministro'}),
        }
        
class MedicoForm(forms.ModelForm):
    class Meta:
        model = Medico
        fields = ['nombrePila', 'apPat', 'apMat', 'cedula', 'numTel']
        widgets = {
            'nombrePila': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del médico'}),
            'apPat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido paterno'}),
            'apMat': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Apellido materno'}),
            'cedula': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Número de cédula'}),
            'numTel': forms.NumberInput(attrs={'class': 'form-control', 'placeholder': 'Número de teléfono'}),
        }
         
class ConsultaForm(forms.ModelForm):
    class Meta:
        model = Consulta
        fields = ['motivo', 'sintomas', 'peso', 'altura', 'diagnostico', 'empleado', 'medicamentos', 'suministros']
        widgets = {
            'motivo': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Motivo de la consulta'}),
            'sintomas': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Síntomas'}),
            'peso': forms.NumberInput(attrs={'class': 'form-control'}),
            'altura': forms.NumberInput(attrs={'class': 'form-control'}),
            'diagnostico': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Diagnóstico'}),
            'empleado': forms.Select(attrs={'class': 'form-control'}),
            'medicamentos': forms.SelectMultiple(attrs={'class': 'form-control'}),
            'suministros': forms.SelectMultiple(attrs={'class': 'form-control'}),
        }
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['medicamentos'].required = False
            
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['suministros'].required = False
        
class EfectosSecundariosForm(forms.ModelForm):
    class Meta:
        model = EfectosSecundarios
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre del Efecto Secundario'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción del Efecto Secundario'}),
        }
        
class ContraindicacionForm(forms.ModelForm):
    class Meta:
        model = Contraindicacion
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la Contraindicación'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción de la Contraindicación'}),
        }

class FormaAdministracionForm(forms.ModelForm):
    class Meta:
        model = FormaAdministracion
        fields = ['nombre', 'descripcion']
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Nombre de la Forma de Administración'}),
            'descripcion': forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Descripción de la Forma de Administración'}),
        }

class AlergiaEmpleadoForm(forms.ModelForm):
    class Meta:
        model = AlergiaEmpleado
        fields = ['alergia', 'empleado']
        widgets = {
            'alergia': forms.Select(attrs={'class': 'form-control'}),
            'empleado': forms.Select(attrs={'class': 'form-control'}),
        }

class MediEfecSecuForm(forms.ModelForm):
    class Meta:
        model = MediEfecSecu
        fields = ['medicamento', 'efectosSecundarios']
        

class MediContraForm(forms.ModelForm):
    class Meta:
        model = MediContra
        fields = ['medicamento', 'contraindicaciones']

class MediFormaAdmiForm(forms.ModelForm):
    class Meta:
        model = MediFormaAdmi
        fields = ['medicamento', 'formaAdministracion']
        
class SuministroConsultaForm(forms.ModelForm):
    class Meta:
        model = SuministroConsulta
        fields = ['suministro', 'consulta', 'cantidad']


class MedicamentoConsultaForm(forms.ModelForm):
    class Meta:
        model = MedicamentoConsulta
        fields = ['medicamento', 'consulta', 'cantidad']