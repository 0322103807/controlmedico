from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta


# Create your models here.

class Alergia(models.Model):
    nombre = models.CharField(max_length=100, null=True)
    descripcion = models.TextField(max_length=255, null=True)

    def __str__(self):
        return self.nombre

class Puesto(models.Model):
    nombre = models.CharField(max_length=30, null=True)
    descripcion = models.TextField(max_length=50, null=True)

    def __str__(self):
        return self.nombre

class Departamento(models.Model):
    nombre = models.CharField(max_length=50, null=True)
    descripcion = models.TextField(max_length=70, null=True)

    def __str__(self):
        return self.nombre

class Empleado(models.Model):
    nombrePila = models.CharField(max_length=20, null=True)
    apPat = models.CharField(max_length=20, null=True)
    apMat = models.CharField(max_length=20, null=True)
    fnacimiento = models.DateField()
    noempleado = models.CharField(max_length=10, null=True)
    puesto = models.ForeignKey('Puesto', on_delete=models.CASCADE)
    departamento = models.ForeignKey('Departamento', on_delete=models.CASCADE, default='')

    alergias = models.ManyToManyField('Alergia', through='AlergiaEmpleado')

    def __str__(self):
        return f"{self.noempleado} {self.nombrePila} {self.apPat} {self.apMat} "

class Medicamento(models.Model):
    nombre = models.CharField(max_length=40)
    descripcion = models.TextField(max_length=40)
    dosis = models.IntegerField()
    stock = models.IntegerField()

    efectos_secundarios = models.ManyToManyField('EfectosSecundarios', through='MediEfecSecu')
    contraindicaciones = models.ManyToManyField('Contraindicacion', through='MediContra')
    formas_administracion = models.ManyToManyField('FormaAdministracion', through='MediFormaAdmi')

    def __str__(self):
        return self.nombre

class Suministro(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField(max_length=50)
    stock = models.IntegerField()

    def __str__(self):
        return self.nombre

class Medico(models.Model):
    nombrePila = models.CharField(max_length=20)
    apPat = models.CharField(max_length=20, null=True)
    apMat = models.CharField(max_length=20, null=True)
    cedula = models.CharField(max_length=10, null=True)
    numTel = models.BigIntegerField()

    def __str__(self):
        return self.nombrePila

class Consulta(models.Model):
    fecha = models.DateField()
    edad = models.IntegerField()
    motivo = models.CharField(max_length=255, null=True)
    sintomas = models.CharField(max_length=255, null=True)
    peso = models.DecimalField(max_digits=5, decimal_places=2)
    altura = models.DecimalField(max_digits=5, decimal_places=2)
    diagnostico = models.CharField(max_length=255, null=True)
    empleado = models.ForeignKey('Empleado', on_delete=models.CASCADE)
    medico = models.ForeignKey('Medico', on_delete=models.CASCADE)

    medicamentos = models.ManyToManyField('Medicamento', through='MedicamentoConsulta')
    suministros = models.ManyToManyField('Suministro', through='SuministroConsulta')

    def __str__(self):
        return str(self.id)

class EfectosSecundarios(models.Model):
    nombre = models.CharField(max_length=30, null=True)
    descripcion = models.TextField(max_length=100, null=True)

    def __str__(self):
        return self.nombre

class Contraindicacion(models.Model):
    nombre = models.CharField(max_length=30, null=True)
    descripcion = models.TextField(max_length=100, null=True)

    def __str__(self):
        return self.nombre

class FormaAdministracion(models.Model):
    nombre = models.CharField(max_length=30, null=True)
    descripcion = models.TextField(max_length=100, null=True)

    def __str__(self):
        return self.nombre

class AlergiaEmpleado(models.Model):
    alergia = models.ForeignKey('Alergia', on_delete=models.CASCADE)
    empleado = models.ForeignKey('Empleado', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.alergia)

class SuministroConsulta(models.Model):
    suministro = models.ForeignKey('Suministro', on_delete=models.CASCADE)
    consulta = models.ForeignKey('Consulta', on_delete=models.CASCADE)
    cantidad = models.IntegerField(default='1')

    def __str__(self):
        return f"{self.suministro} - {self.consulta}"

class MedicamentoConsulta(models.Model):
    medicamento = models.ForeignKey('Medicamento', on_delete=models.CASCADE)
    consulta = models.ForeignKey('Consulta', on_delete=models.CASCADE)
    cantidad = models.IntegerField(default='1')

    def __str__(self):
        return f"{self.medicamento} - {self.consulta}"
    
class MediEfecSecu(models.Model):
    medicamento = models.ForeignKey('Medicamento', on_delete=models.CASCADE)
    efectosSecundarios = models.ForeignKey('EfectosSecundarios', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.medicamento)

class MediContra(models.Model):
    medicamento = models.ForeignKey('Medicamento', on_delete=models.CASCADE)
    contraindicaciones = models.ForeignKey('Contraindicacion', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.medicamento)

class MediFormaAdmi(models.Model):
    medicamento = models.ForeignKey('Medicamento', on_delete=models.CASCADE)
    formaAdministracion = models.ForeignKey('FormaAdministracion', on_delete=models.CASCADE)
    nombre = models.CharField(max_length=30, null=True)

    def __str__(self):
        return str(self.medicamento)